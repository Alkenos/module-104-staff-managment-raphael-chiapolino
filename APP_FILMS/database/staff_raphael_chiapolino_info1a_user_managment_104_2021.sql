-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 02 Juin 2021 à 08:35
-- Version du serveur :  5.6.20-log
-- Version de PHP :  7.0.3

DROP DATABASE IF EXISTS staff_raphael_chiapolino_info1a_user_managment_104_2021;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS staff_raphael_chiapolino_info1a_user_managment_104_2021;

USE staff_raphael_chiapolino_info1a_user_managment_104_2021;



-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 13 Juin 2021 à 18:40
-- Version du serveur :  5.7.11
-- Version de PHP :  7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `staff_raphael_chiapolino_info1a_user_managment_104_2021`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_ranks`
--

CREATE TABLE `t_ranks` (
  `id_ranks` int(255) NOT NULL,
  `ranks` varchar(255) NOT NULL,
  `date_ranks` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_ranks`
--

INSERT INTO `t_ranks` (`id_ranks`, `ranks`, `date_ranks`) VALUES
(5, 'machintruc', '2021-03-31 12:18:23'),
(6, 'dsdshzzhhjh', '2021-04-21 09:55:56'),
(7, 'jhgjhgjgh', '2021-04-21 10:01:55'),
(9, 'fdfdfdhj', '2021-04-28 10:09:16'),
(10, 'ffrf', '2021-05-05 10:10:48'),
(11, 'fefef', '2021-05-05 10:13:15'),
(12, 'jhfrijeijre', '2021-05-05 10:39:15'),
(13, 'vvbfvfd', '2021-06-13 11:20:36'),
(14, 'dfdfdsfdsfdsf', '2021-06-13 11:20:41'),
(15, 'fgfdgfd', '2021-06-13 11:24:59'),
(16, 'mn bngbhnbvnb', '2021-06-13 11:25:15'),
(17, 'hjhhj', '2021-06-13 15:41:35'),
(18, 'ghghg', '2021-06-13 15:42:03'),
(19, 'zjuztjthzj', '2021-06-13 15:49:47');

-- --------------------------------------------------------

--
-- Structure de la table `t_ranks_users`
--

CREATE TABLE `t_ranks_users` (
  `id_ranks_users` int(255) NOT NULL,
  `fk_id_users` int(255) NOT NULL,
  `fk_id_ranks` int(255) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_ranks_users`
--

INSERT INTO `t_ranks_users` (`id_ranks_users`, `fk_id_users`, `fk_id_ranks`, `date_added`) VALUES
(1, 3, 9, '0000-00-00 00:00:00'),
(2, 2, 4, '0000-00-00 00:00:00'),
(3, 1, 10, '2021-05-19 11:07:20'),
(4, 5, 7, '2021-06-13 19:54:30'),
(5, 5, 9, '2021-06-13 19:54:39'),
(6, 5, 4, '2021-06-13 19:54:39'),
(7, 5, 8, '2021-06-13 19:54:48'),
(8, 5, 2, '2021-06-13 19:54:48'),
(9, 5, 3, '2021-06-13 19:56:13'),
(10, 9, 4, '2021-06-13 19:57:27');

-- --------------------------------------------------------

--
-- Structure de la table `t_users`
--

CREATE TABLE `t_users` (
  `id_users` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_users`
--

INSERT INTO `t_users` (`id_users`, `username`, `creation_date`) VALUES
(2, 'michel', '2021-03-02 00:00:00'),
(3, 'quentin', '2021-03-02 00:00:00'),
(4, 'jhgfjhgfjfgjgfjf', '2021-06-13 15:44:51'),
(6, 'ghbghghg', '2021-06-13 16:04:26'),
(7, 'hjhgjhgjhgj', '2021-06-13 16:14:52'),
(8, 'hghghgfhf', '2021-06-13 16:21:46'),
(9, 'hgjhgjhgj', '2021-06-13 16:26:40');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_ranks`
--
ALTER TABLE `t_ranks`
  ADD PRIMARY KEY (`id_ranks`);

--
-- Index pour la table `t_ranks_users`
--
ALTER TABLE `t_ranks_users`
  ADD PRIMARY KEY (`id_ranks_users`),
  ADD KEY `fk_users` (`fk_id_users`),
  ADD KEY `fk_ranks` (`fk_id_ranks`);

--
-- Index pour la table `t_users`
--
ALTER TABLE `t_users`
  ADD PRIMARY KEY (`id_users`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_ranks`
--
ALTER TABLE `t_ranks`
  MODIFY `id_ranks` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT pour la table `t_ranks_users`
--
ALTER TABLE `t_ranks_users`
  MODIFY `id_ranks_users` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `t_users`
--
ALTER TABLE `t_users`
  MODIFY `id_users` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
